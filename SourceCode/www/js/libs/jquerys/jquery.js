//gnb menu
function openNav() {        
	$('#gnb').removeClass('off').addClass('on');
	$('.gnbBg').removeClass('off').addClass('on');
	//setTimeout(navDragEnd, 500);
}
function navDragEnd() {
	/* var previousOffset = null;
	console.log("-------dragable");
	var dragDistance = 100;
	$( "#gnb" ).draggable({
		axis: "x", 
		//containment: "parent",
		drag: function(event,ui){
			console.log("ui.offset.left", ui.offset.left);
			console.log("ui", ui);
			if(ui.offset.left < -100){
				closeNav();
			}
		},
		stop: function( event, ui){ 
		 //console.log(event, ui, "ddd");
			$( "#gnb" ).removeAttr('style');
		}
	}); */
}
function closeNav() {        
	$('#gnb').removeClass('on').addClass('off');
	$('.gnbBg').removeClass('on').addClass('off');
	//$( "#gnb" ).draggable('destroy');
}

//dim layer popup
function openPopup() {        
//	window.location="./main_tunnel2.html"

    $('.dimPopup').addClass('on');
}

function closePopup() {        
	$('.dimPopup').removeClass('on');
}

//dim layer popup
function openPopup2() {
	$('.dimPopup2').addClass('on');
}

function closePopup2() {
	$('.dimPopup2').removeClass('on');
}


//dim layer popup
function openPopup3() {
	$('.dimPopup3').addClass('on');
}

function closePopup3() {
	$('.dimPopup3').removeClass('on');
}

//dim layer popup multi type (한 페이지에 팝업창 여러개 사용할 경우)
function openPopupMulti(e) {
	$('.pop' + e).addClass('on');
}

function closePopupMulti(e) {
	$('.pop' + e).removeClass('on');
}

//탬메뉴 
var tabMenu = function(trigger,content){  
	trigger.each(function(i){
		trigger.eq(i).on('click',function(){
			trigger.removeClass('on');
			$(this).addClass('on');
			content.hide();
			content.eq(i).show();
		});
	});
  }

  $(document).ready(function(){
	//탭메뉴
	tabMenu($('.total .tabs>a'),$('.total .boardContent'));
	tabMenu($('.envSensor .tabs>a'),$('.envSensor .boardContent'));
	tabMenu($('.tabs.type2>a'),$('.tabContentsWrap .tabConSlide'));
	tabMenu($('.bdrBtnsArea .fl a'),$('.subTabCon'));
	
	//응급알람 오버레이 팝업 닫기
    //$('#btnClosePop').on('click',function(){
    $('body').on('click',function(){
        $('.overlay').hide();
	});
	
    $('.gotop').on('click',function(){
        $(document).scrollTop(0);
	});
//	var gnbVerTxt1 = $('.gnbTop table tr:last-child th').text();
//	var gnbVerTxt2 = $('.gnbTop table tr:last-child td').text();
//	$('.gnbMenu').append('<div class="versions">'+gnbVerTxt1+': '+gnbVerTxt2+'</div>')
//	console.log("gnbVerTxt", gnbVerTxt1);

});

function closeDim(){
	$(this).on('click',function(){
		$('.dimPopupWp').hide();
	});
}



/* 20200625 */
$(function(){
	// 상단 스크롤 영역
	var didScroll;
	var lastScrollTop = 0;
	var delta = 5;
	var navbarHeight = $('header').outerHeight();

	$(window).scroll(function(event){
		didScroll = true;
	});

	setInterval(function() {
		if (didScroll) {
			hasScrolled();
			didScroll = false;
		}
	}, 250);

	function hasScrolled() {
		var st = $(this).scrollTop();
		console.log("st", st);
		if(Math.abs(lastScrollTop - st) <= delta)
			return;
		if (st > lastScrollTop && st > navbarHeight){
			// Scroll Down
			$('header').removeClass('nav-down').addClass('nav-up');
			
		} else {
			// Scroll Up
			if(st + $(window).height() < $(document).height()) {
				$('header').removeClass('nav-up').addClass('nav-down');

				console.log("navDown");
			}
		}

		if(st > 50){
			$('.gotop').show();
		}else{
			$('.gotop').hide();
		}
		lastScrollTop = st;
	}

	$('.gnbBg').on('click', function(){
		closeNav();
	})
	$('.goback').on('click', function(){
		history.back(-1);
	})
})

