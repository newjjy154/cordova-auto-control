var varPopUp


function showPopUpShow1(title, msg, bNm1, callback1) {
    showPopUpClose()
    varPopUp = $.confirm({
        backgroundDismissAnimation: 'glow',
        // offsetTop: "50%",
        boxWidth: '70%',
        animation: 'zoom',
        typeAnimated: true,
        animationSpeed: 200, // 0.2 seconds
        theme: 'black',
        bgOpacity: 0,
        title: '<strong style="color:white">' + title + '</strong>',
        content: '<strong style="color:white">' + msg + '</strong>',
        useBootstrap: false,
        cancel: function () {

        },
        buttons: {
            buttonA: {
                text: bNm1,
                action: function () {
                    callback1();
                }
            }
        }
    });
}

function showPopUpShow2(title, msg, bNm1, bNm2, callback1, callback2) {
    showPopUpClose()
    varPopUp = $.confirm({
        backgroundDismissAnimation: 'glow',
        // offsetTop: "50%",
        boxWidth: '70%',
        animation: 'zoom',
        typeAnimated: true,
        animationSpeed: 200, // 0.2 seconds
        theme: 'black',
        bgOpacity: 0,
        title: '<strong style="color:white">' + title + '</strong>',
        content: '<strong style="color:white">' + msg + '</strong>',
        useBootstrap: false,
        cancel: function () {

        },
        buttons: {
            buttonA: {
                text: bNm1,
                action: function () {
                    callback1();
                }
            },
            buttonB: {
                text: bNm2,
                action: function () {
                    callback2();
                }
            }
        }
    });
}

function showPopUpClose() {
    if (varPopUp != null) varPopUp.close();
}


function showBottom(msg) {
    window.plugins.toast.showWithOptions(
        {
            message: msg,
            duration: "short", // which is 2000 ms. "long" is 4000. Or specify the nr of ms yourself.
            position: "center",
            addPixelsY: -40  // added a negative value to move it up a bit (default 0)
        },
        function (e) {

        }, function (e) {

        }
    );
}