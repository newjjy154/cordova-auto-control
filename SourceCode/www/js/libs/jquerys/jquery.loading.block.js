/**
 * jQuery Loading Block
 *
 * demos / documentation: https://github.com/chan15/jquery-loading-block
 *
 * @author Chan
 */
; (function ($) {
    $.loadingBlockShow = function (opts) {
        var defaults = {
            imgPath: 'img/loadingicon2.gif',
            imgStyle: {
                width: '100%',
                display: 'block',
                textAlign: 'center',
                margin: '0 auto',
                marginTop: '60%'
            },
            text: '처리중입니다....',
            style: {
                position: 'fixed',
                width: '100%',
                height: '100%',
                background: 'rgba(255, 255, 255, .0)',
                left: 0,
                top: 0,
                zIndex: 10000
            }
        };
        $.extend(defaults, opts);
        $.loadingBlockHide();

        var img = $('<div><img src="' + defaults.imgPath + '"><div>' + defaults.text + '</div></div>');
        var block = $('<div id="loading_block"></div>');

        block.css(defaults.style).appendTo('body');
        img.css(defaults.imgStyle).appendTo(block);
    };

    $.loadingBlockHide = function () {
        $('div#loading_block').remove();
    };



    $.loadingBlockShowOK = function (opts) {
        var defaults = {
            imgPath: 'img/loadingicon2.gif',
            imgStyle: {
                width: 'auto',
                display: 'block',
                textAlign: 'center',
                margin: '0 auto',
                marginTop: '70%'
            },
            text: '버전체크 진행중 입니다...',
            textStyle: {
                width: '100%',
                display: 'block',
                textAlign: 'center',
                margin: '0 auto',
                marginTop: '70%'
            },
            style: {
                position: 'fixed',
                width: '100%',
                height: '100%',
                background: 'rgba(255, 255, 255, .0)',
                left: 0,
                top: 0,
                zIndex: 10000
            }
        };
        $.extend(defaults, opts);
        $.loadingBlockHideOK();

        var img = $('<div><img src="' + defaults.imgPath + '"><div>' + defaults.text + '</div></div>');
        var block = $('<div id="loading_blockOK"></div>');

        block.css(defaults.style).appendTo('body');
        img.css(defaults.imgStyle).appendTo(block);
    };

    $.loadingBlockHideOK = function () {
        $('div#loading_blockOK').remove();
    };


    $.loadingBlockShow2 = function (opts) {
        var defaults = {
            imgPath: 'img/loadingicon2.gif',
            imgStyle: {
                width: '100%',
                display: 'block',
                textAlign: 'center',
                margin: '0 auto',
                marginTop: '60%'
            },
            text: '데이터를 불러오는 중 입니다...',
            style: {
                position: 'fixed',
                width: '100%',
                height: '100%',
                background: 'rgba(255, 255, 255, 0.0)',
                left: 0,
                top: 0,
                zIndex: 10000
            }
        };
        $.extend(defaults, opts);
        $.loadingBlockHide2();

        var img = $('<div><img src="' + defaults.imgPath + '"><div>' + defaults.text + '</div></div>');
        var block = $('<div id="loading_block2"></div>');

        block.css(defaults.style).appendTo('body');
        img.css(defaults.imgStyle).appendTo(block);
    };

    $.loadingBlockHide2 = function () {
        $('div#loading_block2').remove();
    };


    $.loadingBlockShow3 = function (opts) {
        var defaults = {
            imgPath: 'img/loadingicon2.gif',
            imgStyle: {
                width: '100%',
                display: 'block',
                textAlign: 'center',
                margin: '0 auto',
                marginTop: '60%'
            },
            text: '데이터를 불러오는 중 입니다...',
            style: {
                position: 'fixed',
                width: '100%',
                height: '100%',
                background: 'rgba(255, 255, 255, 0.0)',
                left: 0,
                top: 0,
                zIndex: 10000
            }
        };
        $.extend(defaults, opts);
        $.loadingBlockHide3();

        var img = $('<div><img src="' + defaults.imgPath + '"><div>' + defaults.text + '</div></div>');
        var block = $('<div id="loading_block3"></div>');

        block.css(defaults.style).appendTo('body');
        img.css(defaults.imgStyle).appendTo(block);
    };

    $.loadingBlockHide3 = function () {
        $('div#loading_block3').remove();
    };




}(jQuery));


