package com.sip.linphone;
/*
LinphoneMiniActivity.java
Copyright (C) 2014  Belledonne Communications, Grenoble, France

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;

import org.linphone.core.Address;
import org.linphone.core.Call;
import org.linphone.core.CallParams;
import org.linphone.core.Core;
import org.linphone.core.CoreListenerStub;
import org.linphone.mediastream.video.AndroidVideoWindowImpl;

/**
 * @author Sylvain Berfini
 */
public class LinphoneMiniActivity extends Activity {
    private TextureView mVideoView;
    private TextureView mCaptureView;
    // private AndroidVideoWindowImpl androidVideoWindowImpl;

    private CoreListenerStub mCoreListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Resources R = getApplication().getResources();
        String packageName = getApplication().getPackageName();

        setContentView(R.getIdentifier("incall", "layout", packageName));

        mVideoView = findViewById(R.getIdentifier("videoSurface", "id", packageName));
        mCaptureView = findViewById(R.getIdentifier("videoCaptureSurface", "id", packageName));

        Intent i = getIntent();
        Bundle extras = i.getExtras();
        String address = extras.getString("address");
        String displayName = extras.getString("displayName");
        String masterName = address.substring(4, address.indexOf("@"));

        if (!address.startsWith("sip:")) {
            finish();
            return;
        }

        Core core = Linphone.mLinphoneCore; // 코어 생성
        Address addressToCall = core.interpretUrl(masterName); // 어카운트 관리자 ID
        CallParams params = core.createCallParams(null);
        params.enableVideo(true);
        if (addressToCall != null) {
            core.inviteAddressWithParams(addressToCall, params);
        }

        Linphone.mLinphoneCore.setNativeVideoWindowId(mVideoView);
        Linphone.mLinphoneCore.setNativePreviewWindowId(mCaptureView);

        // 통화상태 변경 리스너
        mCoreListener = new CoreListenerStub() {
            @Override
            public void onCallStateChanged(Core core,
                    Call call,
                    Call.State state,
                    String message) {
                if (state == Call.State.End ||
                        state == Call.State.Released) {
                    finish();
                } else if (state == Call.State.StreamsRunning) {

                }
            }
        };

    }

    public void end(View view) {
        try {
            Core core = Linphone.mLinphoneCore;
            if (core.getCallsNb() > 0) {
                Call call = core.getCurrentCall();
                if (call == null) {

                    call = core.getCalls()[0];
                }
                call.terminate();
            }
            finish();
        } catch (Exception e) {

        }
    }

    // oid switchCamera() {
    // {
    // String videoDeviceId = Linp
    // oDeviceId = (videoDeviceId + 1) % AndroidCameraConfigu
    // L
    // Linphone.mLinphoneManager.upda
    //
    //
    // // window
    // if (mCaptureView != null) {
    // Linphone.mLinphoneCore.setPreviewWindow(mCaptureView);
    // }
    // } catch (ArithmeticException ae) {
    // Log.e("Cannot switch camera : no camera");
    // }
    // }

    @Override
    protected void onResume() {
        super.onResume();
        Linphone.mLinphoneCore.addListener(mCoreListener);
    }

    @Override
    protected void onPause() {

        Linphone.mLinphoneCore.removeListener(mCoreListener);

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        try {
            Core core = Linphone.mLinphoneCore;
            if (core.getCallsNb() > 0) {
                Call call = core.getCurrentCall();
                if (call == null) {

                    call = core.getCalls()[0];
                }
                call.terminate();
            }
        } catch (Exception e) {

        }
        super.onDestroy();
    }
}
