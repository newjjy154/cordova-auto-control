document.addEventListener('deviceready', linphoneManager);

var DEBUGGING = false;


const INDEX_NUMBER = 1;

const STR_DATE_1 = ['login', '로그인'];
const STR_DATE_2 = ['logout', '로그아웃'];
const STR_DATE_3 = ['call', '전화송신'];
const STR_DATE_6 = ['videocall', '화상전화송신'];
const STR_DATE_4 = ['listenCall', '전화대기'];
const STR_DATE_5 = ['acceptCall', '수신동의'];
const STR_DATE_7 = ['hangup', '전화종료'];
const STR_DATE_8 = ['toggleVideo', 'toggleVideo'];
const STR_DATE_9 = ['toggleSpeaker', 'toggleSpeaker'];
const STR_DATE_10 = ['toggleMute', 'toggleMute'];
const STR_DATE_11 = ['sendDtmf', 'sendDtmf'];

const END_MSG_1 = ['Incoming', '대기중.'];
const END_MSG_2 = ['RegistrationSuccess', '성공'];
const END_MSG_3 = ['Connecteding', '대기중.'];
const END_MSG_4 = ['Connected', '완료'];
const END_MSG_5 = ['Error', '실패'];
const END_MSG_6 = ['End', '종료'];
const END_MSG_7 = ['null', ''];
const END_MSG_8 = ['OK', '완료'];
const END_MSG_9 = ['', '에러'];

const POPUP_DATE_1 = ['title', '알림'];
const POPUP_DATE_2 = ['msg', '로그인 하세요.'];
const POPUP_DATE_3 = ['button', '확인'];

const POPUP_DATE_4 = ['title', '알림'];
const POPUP_DATE_5 = ['button', '연결'];
const POPUP_DATE_6 = ['button', '연결종료'];

const POPUP_DATE_7 = ['title', '알림'];
const POPUP_DATE_8 = ['button', '화상전화'];
const POPUP_DATE_9 = ['button', '연결종료'];

const POPUP_DATE_10 = ['title', '알림'];
const POPUP_DATE_11 = ['button', '확인'];

const POPUP_DATE_12 = ['title', '알림'];
const POPUP_DATE_13 = ['button', '연결종료'];

const POPUP_DATE_14 = ['title', '알림'];
const POPUP_DATE_15 = ['button', '화상전화'];
const POPUP_DATE_16 = ['button', '연결종료'];


const POPUP_DATE_17 = ['title', '알림'];
const POPUP_DATE_18 = ['button', '확인'];

const POPUP_DATE_19 = ['title', '알림'];
const POPUP_DATE_20 = ['button', '재연결'];
const POPUP_DATE_21 = ['button', '확인'];


var _isLogine = false
var _isPopUp = false

var linphoneManager = {
    login: function (userNum, serverPw, serverAddr, isPopUp) {
        _isPopUp = isPopUp;
        cordova.plugins.sip.login(userNum, serverPw, serverAddr,
            function (e) {
                if (e == 'RegistrationSuccess') _isLogine = true;
                else _isLogine = false;
                linphoneManager.endMsg(STR_DATE_1[INDEX_NUMBER], e);
            }, function (e) {
                linphoneManager.endMsg(STR_DATE_1[INDEX_NUMBER], e);
            })
    },
    logout: function (userNum, serverPw, serverAddr) {
        cordova.plugins.sip.hangup(function (e) { }, function (e) { })
        cordova.plugins.sip.login(userNum, serverPw, serverAddr, function (e) {
            cordova.plugins.sip.logout(
                function (e) {
                    linphoneManager.endMsg(STR_DATE_2[1], e);
                    _isLogine = false;
                }, function (e) {
                    linphoneManager.endMsg(STR_DATE_2[1], e);
                })
        }, function (e) { })
    },
    call: function (sipNmae, serverAddr, masterNum, userNum) {
        if (_isLogine) {
            linphoneManager.hangup();
            cordova.plugins.sip.call(sipNmae + masterNum + '@' + serverAddr, userNum,
                function (e) {
                    linphoneManager.endMsg(STR_DATE_3[INDEX_NUMBER], e);
                }, function (e) {
                    linphoneManager.endMsg(STR_DATE_3[INDEX_NUMBER], e);
                })
            linphoneManager.endMsg(STR_DATE_3[INDEX_NUMBER], 'Connecteding');
        } else showPopUpShow1(POPUP_DATE_1[INDEX_NUMBER],
            POPUP_DATE_2[INDEX_NUMBER], POPUP_DATE_3[INDEX_NUMBER], function () { });
    },
    hangup: function () {
        if (_isLogine) {
            cordova.plugins.sip.hangup(
                function (e) {
                    linphoneManager.endMsg(STR_DATE_7[INDEX_NUMBER], e);
                }, function (e) {
                    linphoneManager.endMsg(STR_DATE_7[INDEX_NUMBER], e);
                })
            linphoneManager.endMsg(STR_DATE_7[INDEX_NUMBER], 'null');
        } else showPopUpShow1(POPUP_DATE_1[INDEX_NUMBER],
            POPUP_DATE_2[INDEX_NUMBER], POPUP_DATE_3[INDEX_NUMBER], function () { });
    },
    listenCall: function () {
        cordova.plugins.sip.listenCall(
            function (e) {
                linphoneManager.endMsg(STR_DATE_4[INDEX_NUMBER], e);
            }, function (e) {
                linphoneManager.endMsg(STR_DATE_4[INDEX_NUMBER], e);
            })
    },
    acceptCall: function () {
        if (_isLogine) {
            cordova.plugins.sip.acceptCall(true,
                function (e) {
                    linphoneManager.endMsg(STR_DATE_5[INDEX_NUMBER], e);
                }, function (e) {
                    linphoneManager.endMsg(STR_DATE_5[INDEX_NUMBER], e);
                })
        } else showPopUpShow1(POPUP_DATE_1[INDEX_NUMBER],
            POPUP_DATE_2[INDEX_NUMBER], POPUP_DATE_3[INDEX_NUMBER], function () { });
    },
    videocall: function (sipNmae, serverAddr, masterNum, userNum) {
        if (_isLogine) {
            cordova.plugins.sip.videocall(sipNmae + masterNum + '@' + serverAddr, userNum,
                function (e) {
                    linphoneManager.hangup();
                    linphoneManager.endMsg(STR_DATE_6[INDEX_NUMBER], e);
                }, function (e) {
                    linphoneManager.endMsg(STR_DATE_6[INDEX_NUMBER], e);
                })
        } else showPopUpShow1(POPUP_DATE_1[INDEX_NUMBER],
            POPUP_DATE_2[INDEX_NUMBER], POPUP_DATE_3[INDEX_NUMBER], function () { });
    },
    toggleVideo: function () {
        if (_isLogine) {
            cordova.plugins.sip.toggleVideo(
                function (e) {
                    linphoneManager.endMsg(STR_DATE_8[INDEX_NUMBER], e);
                }, function (e) {
                    linphoneManager.endMsg(STR_DATE_8[INDEX_NUMBER], e);
                })
        } else showPopUpShow1(POPUP_DATE_1[INDEX_NUMBER],
            POPUP_DATE_2[INDEX_NUMBER], POPUP_DATE_3[INDEX_NUMBER], function () { });
    },
    toggleSpeaker: function () {
        if (_isLogine) {
            cordova.plugins.sip.toggleSpeaker(
                function (e) {
                    linphoneManager.endMsg(STR_DATE_9[INDEX_NUMBER], e);
                }, function (e) {
                    linphoneManager.endMsg(STR_DATE_9[INDEX_NUMBER], e);
                })
        } else showPopUpShow1(POPUP_DATE_1[INDEX_NUMBER],
            POPUP_DATE_2[INDEX_NUMBER], POPUP_DATE_3[INDEX_NUMBER], function () { });
    },
    toggleMute: function () {
        if (_isLogine) {
            cordova.plugins.sip.toggleMute(
                function (e) {
                    linphoneManager.endMsg(STR_DATE_10[INDEX_NUMBER], e);
                }, function (e) {
                    linphoneManager.endMsg(STR_DATE_10[INDEX_NUMBER], e);
                })
        } else showPopUpShow1(POPUP_DATE_1[INDEX_NUMBER],
            POPUP_DATE_2[INDEX_NUMBER], POPUP_DATE_3[INDEX_NUMBER], function () { });
    },
    sendDtmf: function () {
        if (_isLogine) {
            cordova.plugins.sip.toggleMute('number',
                function (e) {
                    linphoneManager.endMsg(STR_DATE_11[INDEX_NUMBER], e);
                }, function (e) {
                    linphoneManager.endMsg(STR_DATE_11[INDEX_NUMBER], e);
                })
        } else showPopUpShow1(POPUP_DATE_1[INDEX_NUMBER],
            POPUP_DATE_2[INDEX_NUMBER], POPUP_DATE_3[INDEX_NUMBER], function () { });
    },
    endMsg: function (n, e) {
        var msg = '';
        var r;
        if (e == 'Incoming') {
            msg = END_MSG_1[INDEX_NUMBER]
            showPopUpShow2(POPUP_DATE_4[INDEX_NUMBER], n + ' ' + msg,
                POPUP_DATE_5[INDEX_NUMBER], POPUP_DATE_6[INDEX_NUMBER],
                function () {
                    cordova.plugins.sip.accept(true, linphoneManager.endMsg, linphoneManager.endMsg);
                    showPopUpShow2(POPUP_DATE_7[INDEX_NUMBER], n + ' ' + msg,
                        POPUP_DATE_8[INDEX_NUMBER], POPUP_DATE_9[INDEX_NUMBER],
                        function () {
                            linphoneManager.hangup();
                            linphoneManager.videocall();
                        },
                        function () {
                            linphoneManager.hangup();
                        });
                }, function () {
                    linphoneManager.hangup();
                });

        } else if (e == 'RegistrationSuccess') {
            msg = END_MSG_2[INDEX_NUMBER]
            linphoneManager.listenCall();
            if (_isPopUp)
                showPopUpShow1(POPUP_DATE_10[INDEX_NUMBER], n + ' ' +
                    msg, POPUP_DATE_11[INDEX_NUMBER],
                    function () {

                    });

        } else if (e == 'Connecteding') {
            msg = END_MSG_3[INDEX_NUMBER]
            showPopUpShow1(POPUP_DATE_12[INDEX_NUMBER], n + ' ' + msg,
                POPUP_DATE_13[INDEX_NUMBER],
                function () {
                    linphoneManager.hangup();
                });
        } else if (e == 'Connected') {
            msg = END_MSG_4[INDEX_NUMBER]
            showPopUpShow2(POPUP_DATE_14[INDEX_NUMBER], n + ' ' + msg,
                POPUP_DATE_15[INDEX_NUMBER], POPUP_DATE_16[INDEX_NUMBER],
                function () {
                    linphoneManager.hangup();
                    linphoneManager.videocall();
                },
                function () {
                    linphoneManager.hangup();
                });
        } else if (e == 'Error') {
            msg = END_MSG_5[INDEX_NUMBER]
            showPopUpClose();
            linphoneManager.listenCall();
        } else if (e == 'End') {
            msg = END_MSG_6[INDEX_NUMBER]
            showPopUpClose();
            linphoneManager.listenCall();
        } else if (e == 'null') {
            linphoneManager.listenCall();
        } else if (e == 'OK') {
            msg = END_MSG_8[INDEX_NUMBER]
            if (n == STR_DATE_2[INDEX_NUMBER])
                showPopUpShow1(POPUP_DATE_17[INDEX_NUMBER], n + ' ' +
                    msg, POPUP_DATE_18[INDEX_NUMBER],
                    function () {

                    });
        } else {
            msg = END_MSG_9[INDEX_NUMBER]
            linphoneManager.listenCall();
            showPopUpShow2(POPUP_DATE_19[INDEX_NUMBER], n + ' ' + msg,
                POPUP_DATE_20[INDEX_NUMBER], POPUP_DATE_21[INDEX_NUMBER],
                function () {
                    linphoneManager.login();
                },
                function () {

                });
        }
        if (DEBUGGING) showBottom(n + " " + msg);

    },

}
