#! /bin/bash


- move project

cd ./Projects/Aos
pwd

------------------------------------------- Comment below code when not using Linphon. ------------------------------------------- 
# - Copy and paste the source code into the google-services.json, GoogleService-Info.plist project directory.
# cp -R ../../SourceCode/lib/fcm/aos/key/google-services.json ../../Projects/Aos/TestCordovaApp/google-services.json
----------------------------------------------------------------------------------------------------------------------------

cd TestCordovaApp
pwd

------------------------------------------- Comment below code when not using Linphon ------------------------------------------- 
- Add Linphon
cordova plugin add https://github.com/sezerkorkmaz/cordova-plugin-sip.git
# cordova plugin rm cordova-plugin-sip
----------------------------------------------------------------------------------------------------------------------------

cp -R ../../../SourceCode/lib/linphon/aos/native_code/LinphoneMiniManager.java ../../../Projects/Aos/TestCordovaApp/platforms/android/app/src/main/java/com/sip/linphone/LinphoneMiniManager.java
cp -R ../../../SourceCode/lib/linphon/aos/native_code/LinphoneMiniActivity.java ../../../Projects/Aos/TestCordovaApp/platforms/android/app/src/main/java/com/sip/linphone/LinphoneMiniActivity.java
cp -R ../../../SourceCode/lib/linphon/aos/native_code/incall.xml ../../../Projects/Aos/TestCordovaApp/platforms/android/app/src/main/res/layout/incall.xml

cp -R ../../../SourceCode/lib/linphon/aos/native_code/www/linphones ../../../SourceCode/www/js/libs/



- android build
cordova build android









