#! /bin/bash



mkdir ./Projects
mkdir ./Projects/Aos

- move project
cd ./Projects/Aos


- Cordova project creation

cordova create TestCordovaApp com.jjy.test TestCordovaApp



set inputStr_1=TestCordovaApp
set inputStr_2=com.jjy.test
set inputStr_3=TestCordovaApp

- move creation project

cd TestCordovaApp
pwd


- AOS module installation
cordova platform add android@8.0.0

- Browser module installation
cordova platform add browser



- Add app exit URL WabView Contorl
cordova plugin add cordova-plugin-whitelist

- Add app exit ToastMsg Contorl
cordova plugin add cordova-plugin-x-toast

- Add app exit Camera Contorl
cordova plugin add cordova-plugin-camera

- Add app exit InAppBrowser Contorl
cordova plugin add cordova-plugin-inappbrowser

- Add app exit module
cordova plugin add https://github.com/huangang/cordova-plugin-app-exit.git
cordova plugin add cordova-plugin-app-exit

- Add app exit Local storage Contorl
cordova plugin add cordova-plugin-awesome-shared-preferences

- Add app exit file plugin
cordova plugin add cordova-plugin-file

- Add app exit screen orientation
cordova plugin add cordova-plugin-screen-orientation  

- Adding plugin to enable AndroidX in Android module.
cordova plugin add cordova-plugin-androidx

- Adding plugins referencing AndroidX library in Android module.
cordova plugin add cordova-plugin-androidx-adapter


cd ../..
pwd

cp -R ../SourceCode/www ../Projects/Aos/TestCordovaApp/
cp -R ../SourceCode/config.xml ../Projects/Aos/TestCordovaApp/config.xml
cp -R ../SourceCode/lib/res ../Projects/Aos/TestCordovaApp/ 
cp -R ../SourceCode/lib/res/android/native_code/platforms/ ../Projects/Aos/TestCordovaApp/


cd Aos/TestCordovaApp
pwd

- android build
cordova build android









