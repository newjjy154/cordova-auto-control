#! /bin/bash


mkdir ./Projects
mkdir ./Projects/Ios

- move project

cd ./Projects/Ios


- Cordova project creation

cordova create TestCordovaApp com.jjy.test TestCordovaApp


set inputStr_1=TestCordovaApp
set inputStr_2=com.jjy.test
set inputStr_3=TestCordovaApp

- move creation project

cd TestCordovaApp
pwd


- IOS module installation
cordova platform add ios@5.0.1

- Browser module installation
cordova platform add browser


- Add app exit URL WabView Contorl
cordova plugin add cordova-plugin-whitelist

- Add app exit ToastMsg Contorl
cordova plugin add cordova-plugin-x-toast

- Add app exit Camera Contorl
cordova plugin add cordova-plugin-camera

- Add app exit InAppBrowser Contorl
cordova plugin add cordova-plugin-inappbrowser

- Add app exit Local storage Contorl
cordova plugin add cordova-plugin-awesome-shared-preferences

- Add app exit file plugin
cordova plugin add cordova-plugin-file

- Add app exit screen orientation
cordova plugin add cordova-plugin-screen-orientation  

- Add app exit module
cordova plugin add https://github.com/huangang/cordova-plugin-app-exit.git
cordova plugin add cordova-plugin-app-exit



cd ../..
pwd

cp -R ../SourceCode/www ../Projects/Ios/TestCordovaApp/
cp -R ../SourceCode/config.xml ../Projects/Ios/TestCordovaApp/config.xml
cp -R ../SourceCode/lib/res ../Projects/Ios/TestCordovaApp/ 

// cp -R ../SourceCode/lib/res/ios/native_code/Plugins ../Projects/Ios/TestCordovaApp/platforms/ios/TestCordovaApp/


cd Ios/TestCordovaApp
pwd

- iOS build
cordova build ios


cd ../..
pwd

cp -R ../SourceCode/lib/res/ios/icon/Images.xcassets ../Projects/Ios/TestCordovaApp/platforms/ios/TestCordovaApp/
cp -R ../SourceCode/lib/res/ios/native_code/CDVUIWebViewNavigationDelegate.m ../Projects/Ios/CordovaLib/Classes/Private/Plugins/CDVUIWebViewEngine/

cd Ios/TestCordovaApp
pwd








