#! /bin/bash

cp -R ./SourceCode/www ./Projects/Ios/TestCordovaApp/
cp -R ./SourceCode/www ./Projects/Ios/TestCordovaApp/platforms/ios/
cp -R ./SourceCode/config.xml ./Projects/Ios/TestCordovaApp/config.xml
cp -R ./SourceCode/lib/res ./Projects/Ios/TestCordovaApp/ 
cp -R ./SourceCode/lib/res/ios/icon/Images.xcassets ./Projects/Ios/TestCordovaApp/platforms/ios/TestCordovaApp/
cp -a ./SourceCode/lib/res/ios/native_code/Plugins ./Projects/Ios/TestCordovaApp/platforms/ios/TestCordovaApp/

cd ./Projects/Ios/TestCordovaApp
cordova build ios