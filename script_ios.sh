# 실행 할 명령의 주석을 제거후 스크립트 파일을 터미널에서 실행하세요.

# Ios 프로젝트 생성및 라이브러리 추가
# sh Auto/Create/Ios.sh
# sh Auto/Lib-Add/LINPHONE/ADD/Ios.sh
# sh Auto/Lib-Add/FCM/ADD/Ios.sh

# Ios 앱 빌드
sh Auto/Build/Ios/Ios.sh
# sh Auto/Build/Ios/Browser.sh

# Ios 앱 실행
sh Auto/Run/Ios/Ios.sh
# sh Auto/Run/Ios/Browser.sh
